---
title: 'Introduction to R: Core Language Tutorial'
author: "Timothée Vincent"
output:
  pdf_document: default
  html_document: default
---
## First steps in R

```{r include = FALSE}
#install.packages("rmarkdown")
library("rmarkdown")
```

### R can be used as a calculator
```{r basic mathematics operations}
# additions
5 + 5

# substractions
8 - 3

# multiplications
7 * 8

# divisions
13 / 3

# modulo (finds the remainder of the division)
9 %% 3

9 %% 4

# exponent
2 ** 3

# square root
sqrt(4)

# absolute value
abs(-4.5)
abs(8)

# exponential function
exp(9)

# logarithms
log(8) # natural log, the one called "ln"

log(8, base = 10) # log10

# more complex operations with the use of parenthesis
(4 + 4) * 2 
4 + (4 * 2)

# We can test equality. It gives boolean variables:
# TRUE and FALSE, T and F can be used as abreviations

TRUE

FALSE

# Equality (return TRUE if equal)
2 == 2
2 == 1 # We use "==" to test equality

# Inequality (return TRUE if not equal)
2 != 1
2 != 2 # we use "!=" to test inequality

# Greater than ">"
10 > 3 
10 < 3

# Greater than or equal
10 > 10
10 >= 10

# Less than
10 < 5

# Less than or equal 
10 <= 5

## But we also can create vectors : 
c(1,2,3,4,5)
# a vector is created with the use of c(), a function which concatenate its argument end to end.

# Matrix 
matrix(c(1,2,3,4,5,6), ncol = 2, byrow = FALSE) # Matrix with two columns, filled column by column
matrix(c(1,2,3,4,5,6,7), ncol = 2, byrow = FALSE) # Must have the same dimensions, if not, the result can be surprising

## R manages character chains
"Hello world"
# A character chain must start and end with " or '
```
### Help
It's possible to get more information on any function. 

```{r}
# To get help on a specific function
# help(name_of_the_function)
help(sum)
# or
?sum # Same result, but shorter syntax

# Do a full text search on R help
# The help.search command (alternatively ??) allows searching for help in various ways. For example
??mean
  
# And don't forget, Google is your friend !

```

### How to assign a value to a variable

```{r} 
########################################################
# Assignment
# We assign a value to a variable using <- or =
# <- is the common way to assign a value
a <- 3 
# Nothing appear, but now, a contains the value 3
a

a <- 2 + 2 # Now "a" contains the value 4

# We can do operation with the variable itself
a <- a + 2
a 

# We can also assign a value using -> (but not recommended)
2 -> b
b 
```

``` {r}
########################################################
# Variable name rules : 
# There are some rules to respect
# 1. Variable name start with a letter (lower or uppercase)
# 2. Variable name can contain numbers
# 3. Variable name can contain special characters (but not "" or -)
# 4. Variable name is case sensitive

# this works 
firstVariable <- 1
variable1 <- 1
b_b <- 3
.hidden <- 1 # Hidden variable

# this doesn't work
#b-b <- 2 # Doesn't work, here b-b means b - b, it's a subtraction
# my variable <- "Hello"
# 123 <- 123

# And those variables are differents
b <- 2
B <- 3
b
B



########################################################
# Spaces. 
# R is quite permissive with the use of spaces, good use of spaces improves code readability.
a <- 2 # Good
a<-2 # same operation, but more noisy
b <- c(      1,2,3         ,  4) # Bad but works
b <- c(1, 2, 3, 4) # Good

########################################################
# Multi-line commands
# Commands can generally span multiple lines
# as long as R does not think the command has finished
a <- c(1, 2,
       3, 4) # this works
a
# This doesn't work
b <- 2
    + 2 
b
# But this works 
b <- 2 + 
  2
b
########################################################
# Comments
# Comments are any text on a line following a hash #
# Used as first character of the line to describe the following lines of code
# Or at the end of a common line of code


########################################################
# Remove objects : 
# One command : rm
rm(a)


# Remove several objects at the same time
rm(b, firstVariable)
```

### Data types: logical, character, numeric
Object can have different type of nature. The main ones are

* Numeric : double and integer
* Boolean : `TRUE` or `FALSE`
* character : `"a"`, `"Hello"`
* *factor* : they are used to represent categorical data. Factors can be ordered or unordered and are an important class for statistical analysis and for plotting.

```{r}
########################################################
# Numeric types
# An integer is, by definition, integer. 
a <- 1
# Double is for double precision floating point numbers
b <- 1.2
## they are both numeric

# We will use the function typeof to see the nature of those variables
# What is typeof ? mode ? class ? 
# Get some help 
?typeof
?mode
?class
typeof(a); mode(a); class(a)
typeof(b)
# It's the same. All integers are doubles, but all doubles are not integers. 
# We can force R to have integers
a <- as.integer(a)
typeof(a); mode(a); class(a)

########################################################
# Factors
fac <- factor(c(1, 1, 1, 2, 2, 2))
fac # Factors have a reference level, classified by alphabetical or numerical order. 
fac2 <- factor(c("mutant", "WT","mutant", "WT"))
fac2 
# Mutant is the reference factor. If we want to have WT as reference, we have to specified it.
# Function relevel 
?relevel
fac2 <- relevel(fac2, "WT")
fac2
# Now, Wt is the reference factor

## Converting factors
# From factor to doubles 
fac_double <- as.double(fac)
fac_double

# From factors to characters
fac2_char <- as.character(fac2)
fac2_char

# And from doubles or characters to factors
fac <- as.factor(fac_double)
fac
fac2 <- as.factor(fac2_char)
fac2
```
Note:  There is a large collection of functions of the form as.something() for either coercion from one mode to another, or for investing an object with some other attribute it may not already possess. 

### Basic data structures: Vectors, Matrices, Lists, Data.frames
The most common structures in R are : 

* *vector* is the most important one.
* *matrices* or more generally arrays are multi-dimensional generalizations of vectors. In fact, they are vectors that can be indexed by two or more indices and will be printed in special ways.
* *lists* : store arbitrary structures of one or more named elements.
* *data frames* are matrix-like structures, in which the columns can be of different types. Think of data frames as ‘data matrices’ with one row per observational unit but with (possibly) both numerical and categorical variables. Many experiments are best described by data frames: the treatments are categorical but the response is numeric.
* specific objects generated by a function. 

```{r}
###########################################################
# Vectors:
# In R, a single value (scalar) is a vector. 

x <- 1 # I.e., x is a vector of length 1
# In addition to importing data,
# R has various functions for creating vectors. 
c(1, 2, 3, 4) # c stands for combine

1:10 # create an integer sequence 1 to 10
seq(1, 10) # alternative way of creating a sequence
seq(1, 10, by = 2) # The function has additional options
rep(1, 5) # repeat a value a certain number of times
rep(c(1,2,3), 5) # repeat a value a certain number of times
# Vectors can have names
x <- c(1,2,3,4,5)
names(x) <- c("a", "b", "c", "d", "e") 
x

# Extracting vectors
x[c(1,2)] # by numeric position

x[x < 3] # by logical vector
x[c("b", "c")] # by name

# vector length 
length(x)

# We can add 1 to all the vector elements
x + 1
# We can add two vectors
x + c(6,7,8,9,10)
# If the vectors don't have the same length, the smalest one wil be repeated as much as necesary
x + c(0, 1)

# Vector can be sorted : 
x <- c(9, 8, 25, 67, 3)
sort(x)
sort(x, decreasing = TRUE)

# order() will return the numeric position
order(x)
x[order(x)]
###########################################################
# Matrices:
# All data must be of same type (e.g., numeric, character, logical) 
y <- matrix(c(1, 2,
            4, 5,
            7, 8 ),
            byrow = TRUE, ncol = 2)
y
# number of rows and columns
class(y)
dim(y) # Number of rows and columns
nrow(y) # Number of rows
ncol(y) # Number of columns
# Rows and columns can be given names
rownames(y) <- c("a", "b", "c")
colnames(y) <- c("col1", "col2")

# Rows and columns can be indexed
y["a", ] # By rowname
y[, "col1"] # By column name 
y["a", "col1"] # By both

y[c(1,2), ] # By row position
y[,1] # By column position
y[c(2,3), 2] # By column position

###########################################################
# Lists
# Store arbitrary structures of one or more named elements.
# Elements can be of different lengths
# Lists can contain lists can be nested to create tree like structures
# Lists are commonly used for representing results of analyses

w <- list(apple = c("a", "b", "c"), 
          banana = c(1,2), 
          carrot = FALSE,
          animals = list(dog = c("dog1", "dog2"), 
                         cat = c(TRUE, FALSE)))

class(w)

# Accessing one element of list
w$apple # using dollar notation
w[[1]] # by position
w[["apple"]] # by name (double brackets)

# Accessing subset of list
w[c(1, 2)]  # by position (single bracket)
w[c("apple", "banana")] # by name
w[c(FALSE, FALSE, TRUE, TRUE)] # by logical vector


```

```{r}
###########################################################
# Data Frames:
# Data frames are the standard data strucure used for storing
# data. If you have used other software (e.g., SPSS, Excel, etc.), 
# this is what you may think of as a "dataset".
# Columns can be of different data types (e.g., character, numeric, logical, etc.)
z <- data.frame(var1 = 1:9, var2 = letters[1:9])
z

# Tip: Some functions work with matrices, 
#   some work with data.frames, 
#   and some work with both.
# * If you are wanting to store data like you might store in 
#   a database, then you'll generaly want a data.frame.
# * If you are dealing with a mathematical object that you
#   you want to perform a mathematical operation on, then you generally
#   want a matrix (e.g., correlation matrix, covariance matrix, 
#   distance matrix in MDS, matrices used for matrix algebra).
```
### Working directory
R is always pointed at a directory on your computer. You can find out which directory by running the `getwd()` (get working directory) function; this function has no arguments. The current working directory is also displayed by RStudio within the title region of the Console.
getwd()



setwd(dir)
* dir – Specify a working directory. This may be from the root directory (starting with / on a Mac), it may include a double-dot (..) to move locally up a folder from the current directory, and it may include a path from the current directory.


 There are a number of ways to change the current working directory:

* Use the `setwd()` R function
* Use the Tools | Change Working Dir... menu (Session | Set Working Directory on a mac). This will also change directory location of the Files pane.
* From within the Files pane, use the More | Set As Working Directory menu. (Navigation within the Files pane alone will not change the working directory.)

Be careful to consider the side effects of changing your working directory:

* Relative file references in your code (for datasets, source files, etc) will become invalid when you change working directories.
* The location where .RData is saved at exit will be changed to the new directory.

Because these side effects can cause confusion and errors, it's usually best to start within the working directory associated with your project and remain there for the duration of your session. The section below describes how to set RStudio's initial working directory.

```{r}
getwd()
```

### Conditions
Decision making is an important part of programming. This can be achieved in R programming using the conditional `if...else` statement.


#### R if statement
The syntax of if statement is:
```r
if (test_expression) {
   statement
}
```
If the test_expression is `TRUE`, the statement gets executed. But if it's `FALSE`, nothing happens.

Here, test_expression can be a logical or numeric vector, but only the first element is taken into consideration.

In the case of numeric vector, zero is taken as `FALSE`, rest as `TRUE`.

```{r}
# Example of if...else statement
x <- 5
if(x > 0){
   print("Positive number")
}
# you can try with a negative number, nothing will happen
```
#### if...else statement
The syntax of if...else statement is:
```r
if (test_expression) {
   statement1
} else {
   statement2
}
```
The else part is optional and is only evaluated if test_expression is `FALSE`.

It is important to note that else must be in the same line as the closing braces of the if statement.
```{r}
# Example of if...else statement
x <- -5
if(x > 0){
   print("Non-negative number")
} else {
   print("Negative number")
}
```
The above conditional can also be written in a single line as follows.
```{r}
if(x > 0) print("Non-negative number") else print("Negative number")
```

#### Nested if...else statement
We can nest as many `if...else` statement as we want as follows.

The syntax of nested `if...else` statement is:
```r
if ( test_expression1) {
   statement1
} else if ( test_expression2) {
   statement2
} else if ( test_expression3) {
   statement3
} else
   statement4
```

Only one statement will get executed depending upon the test_expressions.
```{r}
# Example of nested if...else
x <- 0
if (x < 0) {
   print("Negative number")
} else if (x > 0) {
   print("Positive number")
} else
   print("Zero")
```

### Functions
A function is a piece of code written to carry out a specified task; it can or can not accept arguments or parameters and it can or can not return one or more values.

There are several possible formal definitions of ‘function’ spanning from mathematics to computer science. Generically, its arguments constitute the input and their return values their output.

#### Base R package functions
Lots of functions are present with the base R package. [Here](https://stat.ethz.ch/R-manual/R-devel/library/base/html/00Index.html) is the list. It has already a lot of mathematical functions implemented. 

```{r}
# Basic mathematical functions
a <- c(1,2,3,4,5,6)
sum(a) # sum all numbers contain in the vector a
var(a) # variance of the a vector
sd(a) # standard deviation
sqrt(4) # squared root of 4

# summary() is the all in one descritptive function. Very useful. It changes depending on th class of the given argument.

```
But also statistical functions : 
```{r}
?rnorm
norm_dif <- rnorm(n = 10, mean = 3, sd = 2) # random generation of ten values for the normal distribution with mean equal to 3 and standard deviation equal to 2
?t.test() # Statistical tests
t.test(norm_dif) # Student test : is the vector's mean equal to zero ? 

```

There are functions specific to a type. 
```{r}
# Functions specifics to string manipulation
phrase <- "good morining england"
substr(phrase, start = 2, stop = 7) # extract the substring between the second and seventh positions in a string. 
 A <- "Burger"; sname <- "King"
paste(A, B) # Two values are concatenated with paste function
paste(A, B, sep = "-") # We can change the separater.
toupper("BoNjouR")
tolower("AuREVoir")

length(A) # Give the number of objects
nchar(A) # Give the number of characters
# More string manipulation functions can be found in R documentation
help("sub")
```

#### Packages
Packages are collections of R functions, data, and compiled code in a well-defined format. The directory where packages are stored is called the library. R comes with a standard set of packages. Others are available for download and installation. Once installed, they have to be loaded into the session to be used. 
There is currently 11731 available packages on the CRAN package repository.

List of CRAN available packages : https://cran.r-project.org/web/packages/

In bioinformatic, we use a lot of [bioconductor](https://www.bioconductor.org/) packages (like DESeq2, edgeR...).

```{r}
# How to install and run an R package : 
#install.package("ade4")
#library(ade4)

# how to install and run a bioconductor package 
#source("https://bioconductor.org/biocLite.R")
#biocLite("DESeq2")
#library("DESeq2")
```

#### Back to the functions
The `data.table` package

#### User Defined Functions
Whether you need to accomplish a particular task and are not aware that a dedicated function or library exists already or because by the time you spend googling for some existing solution, you can have already come out with your own, you will find yourself at some time typing something like:
```r
function.name <- function(arguments)
{
computations on the arguments
return sth (or not)
}
```
```{r}
# Example of basic function
square <- function(x)
{
  return (x ** 2)
}
square(2)

# In R, you don't have to specify what you want to return, by default, it will return the last line of code
square <- function(x)
{
  x ** 2
}
square(3)

# Function with multiple parameter
hypothenus <- function(a, b)
{
  squareA <- a ** 2
  squareB <- b ** 2
  hypo <- sqrt(squareA + squareB)
  hypo
}
hypothenus(3,4)

# Multiple way to create the same function :
hypothenus.b <- function(a,b)
{
  squareA <- square(a)
  squareB <- square(b)
  sqrt(squareA + squareB)
}
hypothenus.b(3, 4)

hypothenus.c <- function(a,b)
{
  sqrt(a ** 2 + b ** 2)
}
hypothenus.c(3, 4)
# The last one is prettier and easier to understand
```

#### Particular functions
##### The apply functions as alternatives to loops
The `apply()` family pertains to the R base package and is populated with functions to manipulate slices of data from matrices, arrays, lists and dataframes in a repetitive way. These functions allow crossing the data in a number of ways and avoid explicit use of loop constructs. They act on an input list, matrix or array and apply a named function with one or several optional arguments.

The called function could be:

* An aggregating function, like for example the mean, or the sum (that return a number or scalar);
* Other transforming or subsetting functions; and
* Other vectorized functions, which return more complex structures like lists, vectors, matrices and arrays.


The apply() functions form the basis of more complex combinations and helps to perform operations with very few lines of code. More specifically, the family is made up of the `apply()`, `lapply()`, `sapply()`, `vapply()`, `mapply()`, `rapply()`, and `tapply()` functions.

But how and when should we use these?

Well, this depends on the structure of the data that you want to operate on and the format of the output that you need.

`apply()`
The `apply()` function operates on arrays (like matrix). 
It will operate a function to each row or column (you can chose), like sum, for example.
```{r}
?apply
mat <- aa <- matrix(c(1,2,3,4,5,6,7,8), ncol = 2, byrow = FALSE)

# To sum all the columns : 
apply(aa, 2, sum)
# To sum all the rows : 
apply(aa, 1, sum)

# To have all the element as squares
apply(aa, 2, square) # row by row
apply(aa, 1, square) # Col by col
```

`sapply()` apply a choosen function to each element of a vector. It will return a vector.
```{r}
x <- c(8,9,6,5,8)
sapply(x, sqrt)

# If function called has more than one argument, you can pass the additionnal arguments
sapply(x, hypothenus, b = 3)
#sapply(x, hypothenus) doesn't work
y <- c("E","R", "a","ToTo")
sapply(y, tolower)


```

`lapply()` apply a choosen function to each element of a list. It will return a list.
```{r}
x <- list(a = 1:10, beta = exp(-3:3), logic = c(TRUE,FALSE,FALSE,TRUE))
lapply(x, mean)
mean(c(TRUE,TRUE))
mean(c(FALSE,FALSE))
# Boolean are computed as 0 (FALSE) and 1 (TRUE)
```

#### rbind and cbind
`rbind()` function combines vector, matrix or data frame by rows.

```r
rbind(x1,x2,...)
# x1,x2:vector, matrix, data frames
```

```{r}
z <- data.frame(var1 = 1:9, var2 = letters[1:9])
z
# new dataframe 
z.2 <- data.frame(var1 = seq(1, 10, by = 2) , var2 = toupper(letters[1:5]))
z.2

rbind(z, z.2)

# The columns of the two datasets must be same, otherwise the combination will be meaningless. 
```

`cbind()` function combines vector, matrix or data frame by columns.
```{r}
cbind(z, rep(1,9))
cbind(z, var3 = rep(1,9)) # We can name the third column
# cbind(z, var3 = rep(1,5)) # Don't have the same length, it doesn't work
```

#### loading data
Reads a file in table format and creates a data frame from it, with cases corresponding to lines and variables to fields in the file. 

```{r}
?read.table
A <- read.table("./data/table_1.txt", h = T)
names(A)
summary(A)
str(A)
```

#### list of others useful functions
```{r}
# Statistical function
?mean
?var
?sd
?median

?t.test
?shapiro.test
?wilcox.test

# Example of statistical test output : 
a <- c(5,4,6,8,2,5,6)
b <- c(7,5,6,9,5,4,6,8,6,3)
t.test(a,b)
res <- t.test(a,b)
str(res)
names(res)
summary(res)
```

#### List of graphical functions 
```{r}
?plot
?barplot
?hist
?lines
?points
?legend
?abline

# Graphics in action : with iris data, included in R
attach(iris)
names(iris)
head(iris)

# Basic plot : x = Petal.length, y = Petal.Width
plot(iris$Petal.Length, iris$Petal.Width, main="Edgar Anderson's Iris Data")
# Basic plot : x = Petal.length, y = Petal.Width, point shape according to iris Species
plot(iris$Petal.Length, iris$Petal.Width, pch=c(23,24,25)[unclass(iris$Species)], main="Edgar Anderson's Iris Data")
# Basic plot : x = Petal.length, y = Petal.Width, color according to iris Species
plot(iris$Petal.Length, iris$Petal.Width, pch=21, bg=c("red","green3","blue")[unclass(iris$Species)], main="Edgar Anderson's Iris Data")

# Scatter plot, all variables against all variables
pairs(iris[1:4], main = "Edgar Anderson's Iris Data", pch = 21, bg = c("red", "green3", "blue")[unclass(iris$Species)])

# histogram of iris Length
hist(iris$Petal.Length)

# Boxplot according to iris species
boxplot(iris$Sepal.Length ~ iris$Species)
boxplot(iris$Sepal.Length ~ iris$Species, notch = T)
boxplot(iris$Sepal.Length ~ iris$Species, notch = T,
        xlab = "Species",
        ylab = "Sepal Length",
        main = "Sepal Length by Species in Iris") 
```


# Unix pour les biologistes

### Workshop : NGS data analysis on the command line
### Valérie Cognat
### Day 1 - 2017-11-13




## Pourquoi Unix?

[Unix] est un système d'exploitation qui a vu le jour à la fin des années 60. Il n'a cessé d'être développé depuis. C'est un sytème stable, multi utilisateurs et multi tâches, utilisé aussi bien pour des serveurs, des ordinateurs de bureau ou des portables. Plusieurs variantes d'Unix existent aujourd'hui, avec, entre autres, [Linux] et [MacOs]. Les différences entre elles sont négligeables à notre niveau.

[Unix]: http://fr.wikipedia.org/wiki/Unix
[Linux]: http://fr.wikipedia.org/wiki/Linux
[MacOs]: https://fr.wikipedia.org/wiki/MacOS

La plupart des outils bioinformatiques sont developpés pour des ordinateurs avec un système d'exploitation Unix. Ce pourquoi il est indispensable de connaitre les bases afin de pouvoir utiliser ces outils.

L'objectif de ce cours est de vous donner les commandes qui vont vous permettre de travailler sous Unix. Ces commandes permettent de lancer différentes tâches et d'obtenir des résultats.


## Le Terminal ##

Le **terminal** est un fenêtre d'invite de commandes qui va vous permettre de naviguer dans le systèmes d'exploitation, de visualiser des fichiers et d'exécuter des programmes. Toutes les machines "unix" disposent d'un terminal.

Quand vous executer l'application "terminal", vous êtes toujours dans votre répertoire *home*. Ce répertoire vous est personnel, il contient vos propres fichiers et repertoires. C'est votre répertoire de travail.


## Les lignes de commandes Unix ##

Une ligne de commande permt d'exécuter un programme avec ses paramètres.
Les commandes sont interprétées par un programme -  dit "shell" - qui est l'interface entre Unix et l'utilisateur. Nous utilisons le shell *bash* (il en existe d'autres).  
Typiquement, chaque commande est tapé sur une ligne puis executé en appuyant sur la touche Entrez sur le clavier.  
Les commandes permettent de manipuler les fichiers et les programmes, *i.e.* :
- demander des informations (ex: lister les fichiers d'un répertoire)
- executer une tâche simple (ex: renommer un fichier)
- exécuter un programme bioinformatique (ex: un alignement avec bowtie, IGV, ...)
- arrêter une application

Vous trouverez toute la documentation souhaitée sur le Web. Les commandes les plus utiles sont récapitulées sur la [fiche suivante]. Gardez en une copie sous la main le temps de vous familiariser avec Unix.

[fiche suivante]: unix_linux_command_reference.pdf
![command reference sheet](unix_linux_command_reference.png)

Une dernière information : Unix est sensible à la casse ! Ce terme signifie que l'écriture utilisée (majuscule/minuscule) change la signification !

# Workshop: NGS data analysis on the command line

**IBMP, Strasbourg - 13, 14 and 15 November 2017**

### Overview

During this 3-day workshop we will introduce you to the Linux environment, to shell commands and to basic R scripting. Using these **command-line skills (Day 1)**, we will then embark on two NGS data analyses -- **small RNA-seq (Day 2)** and **RNA-seq (Day 3)** -- based on published datasets from the organism *Arabidopsis thaliana*.

### Instructors

* (Da)vid Pflieger
* (Hé)lène Zuber
* (Ma)lek Alioua
* (Sa)ndrine Koechler
* (St)éfanie Graindorge
* (Ti)mothée Vincent
* (To)dd Blevins
* (Va)lérie Cognat

## Locations

All trainings will be held in room 243.

## Requirements

A laptop with at least 8Go RAM and 150Go free space on the hard drive.
[VirtualBox](https://www.virtualbox.org/) in order to load the CentOS7 Virtual Machine.

## Schedule

⚠️ Lunch is not provided but there will be free coffee available

### Day 1 (Monday, 13 Nov.) - Unix for biologists, Intro to R

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 09:00 | **Welcome and Introduction to Unix environment** | Ti + Va |
| 10:00 | Morning Break | |
| 10:30 | **Introduction to command line (bash)** | Ti + Va |
| 12:00 | Lunch, *on your own* | |
| 13:30 | **Introduction to R: Core Language Tutorial** | Ti + Va |
| 15:30 | Afternoon Break | |
| 16:00 | **Statistics (normal law, t.test, parametric/non-parametric test)** | Ti + Va |
| 18:00 | Close Day 1 | |

### Day 2 (Tuesday, 14 Nov.) - NGS and small RNA-seq fundamentals

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 09:00 | **Introduction to Next-Generation Sequencing (NGS) technologies** | Sa + Ma |
| 10:30 | Morning Break | |
| 11:00 | **small RNA-seq data quality control & trimming** | Da + To |
| 12:00 | Lunch, *on your own* | |
| 13:30 | **small RNA-seq alignment to reference genome** | Da + To |
| 15:00 | Afternoon Break | |
| 15:30 | **small RNA-seq visualization and analysis (JBrowse, R and ggplot2)**| Da + To |
| 17h30 | Close Day 2 | |

### Day 3 (Wednesday, 15 Nov.) - RNA-seq analysis

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 09:00 | **Introduction to RNA-seq data acquisition and preprocessing of RNA-seq reads** | Hé + St |
| 10:30 | Morning Break | |
| 11:00 | **Mapping of RNA-seq data and preparing files for Differential Expression analysis** | Hé + St |
| 12:00 | Lunch, *on your own* | |
| 13:30 | **Analysis of *A.thaliana* RNA-seq data** | Hé + St |
| 15:30 | Afternoon Break | |
| 16:00 | **IGV and visualization tools** | Hé + St |
| 17:30 | Close Day 3 | |